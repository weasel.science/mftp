FROM alpine:3.12 AS downloader
WORKDIR /root
RUN wget https://www.monstaftp.com/downloads/monsta_ftp_2.10.3_install.zip
RUN unzip monsta_ftp_2.10.3_install.zip

FROM php:7.2-apache
COPY --from=downloader /root/mftp/ /var/www/html/